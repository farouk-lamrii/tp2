package fr.application_test.tqdx1108.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    protected Button ajouter, lister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ajouter = (Button)findViewById(R.id.ajouter);
        lister = (Button)findViewById(R.id.lister);

        ajouter.setOnClickListener(this);
        lister.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Button b = (Button)view;
        String text = b.getText().toString();
        Log.d("button",text);
        switch(text){

            case "Ajouter un étudiant" :
                Intent ajouter = new Intent(MainActivity.this, ajouter.class);
                startActivity(ajouter);
                break;

            case "Lister les étudiants" :
                Intent lister = new Intent(MainActivity.this, lister.class);
                startActivity(lister);
                break;
        }
    }
}
