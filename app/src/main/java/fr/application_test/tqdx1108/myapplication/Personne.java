package fr.application_test.tqdx1108.myapplication;

import java.util.ArrayList;
import java.util.List;

public class Personne {
    private String prenom,nom,mail,sexe;
    public static ArrayList<Personne> personnes = new ArrayList<Personne>();;

    public Personne(String prenom, String nom, String mail) {
        this.prenom = prenom;
        this.nom = nom;
        this.mail = mail;
        this.sexe = sexe;
    }

    public Personne() {
    }

    public static void addPersonne(Personne p){
        personnes.add(p);
    }

    public static List<Personne> getListPersonne(){
        return (personnes);
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }
}
