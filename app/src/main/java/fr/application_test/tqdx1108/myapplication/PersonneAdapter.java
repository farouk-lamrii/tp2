package fr.application_test.tqdx1108.myapplication;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class PersonneAdapter extends ArrayAdapter<Personne> {
    public PersonneAdapter(Context context, List<Personne> objects) {
        super(context, 0, objects);
    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            LayoutInflater inflater =
                    (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.activity_lister, parent, false);
        }
        Personne personne = getItem(position);
        TextView Titre = (TextView)view.findViewById(R.id.nom);
        Titre.setText(personne.getNom() + " - " + personne.getPrenom());
        return view;
    }
}
