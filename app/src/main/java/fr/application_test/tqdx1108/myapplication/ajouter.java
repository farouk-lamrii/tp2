package fr.application_test.tqdx1108.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import java.util.List;

public class ajouter extends AppCompatActivity implements View.OnClickListener{
    private EditText nom,prenom,mail;
    private Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajouter);

        nom = (EditText) findViewById(R.id.nom);
        prenom = (EditText) findViewById(R.id.prenom);
        mail = (EditText) findViewById(R.id.email);

        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
            Log.d("ok","ok");

            Personne p = new Personne(nom.getText().toString(),prenom.getText().toString(),mail.getText().toString());
            Personne.addPersonne(p);
        Intent intent = new Intent(ajouter.this, lister.class);
        startActivityForResult(intent,1);
    }
}
