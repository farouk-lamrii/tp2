package fr.application_test.tqdx1108.myapplication;

import android.app.ListActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class lister extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("liste","lister"+Personne.getListPersonne().get(0).getPrenom());

        List<Personne> personnes = Personne.getListPersonne();
        PersonneAdapter aa = new PersonneAdapter(this,personnes);
        setListAdapter(aa);
    }
}
